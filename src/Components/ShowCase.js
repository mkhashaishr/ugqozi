import React from "react";
import { Container, Row, Col, Card, Stack } from "react-bootstrap";
import imgSc1 from "../Assets/Img/Show case/1.png";
import imgSc2 from "../Assets/Img/Show case/2.png";
import imgSc3 from "../Assets/Img/Show case/3.png";
import imgSc4 from "../Assets/Img/Show case/4.png";
import imgSc5 from "../Assets/Img/Show case/5.png";
import imgSc6 from "../Assets/Img/Show case/6.webp";
import imgSc7 from "../Assets/Img/Show case/7.jpeg";
import imgSc8 from "../Assets/Img/Show case/8.jpeg";
import imgSc9 from "../Assets/Img/Show case/9.jpeg";
import imgSc10 from "../Assets/Img/Show case/10.jpeg";
import imgSc11 from "../Assets/Img/Show case/11.webp";
import imgSc12 from "../Assets/Img/Show case/12.webp";
import imgSc13 from "../Assets/Img/Show case/13.jpeg";
import imgSc14 from "../Assets/Img/Show case/14.jpeg";
import imgSc15 from "../Assets/Img/Show case/15.jpeg";
import imgSc16 from "../Assets/Img/Show case/16.webp";
import imgSc17 from "../Assets/Img/Show case/17.jpeg";
import imgSc18 from "../Assets/Img/Show case/18.jpeg";
import imgSc19 from "../Assets/Img/Show case/19.webp";
import imgSc20 from "../Assets/Img/Show case/20.jpeg";
import imgSc21 from "../Assets/Img/Show case/21.jpeg";
import imgSc22 from "../Assets/Img/Show case/22.jpeg";
import imgSc23 from "../Assets/Img/Show case/23.jpeg";
import imgSc24 from "../Assets/Img/Show case/24.jpeg";
import imgSc25 from "../Assets/Img/Show case/25.jpeg";

const ShowCase = () => {
    return (
        <div className="showCase-section">
            <Container fluid>
                <Row className="justify-content-end">
                    <div className="showCase-section--wrapper position-relative">
                        <h1 className="text-header mb-5 text-start">Show Case</h1>
                        <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel" data-bs-interval="false">
                            <div className="carousel-inner">
                                <div className="carousel-item active">
                                    <div className="d-md-inline-flex flex-wrap flex-xl-nowrap justify-content-center jutify-content-xl-start gap-1 p-0 g-0">
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc1} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Movie</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc2} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Music</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc3} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Writing</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc4} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Music</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc5} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Podcast</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <div className="d-md-inline-flex flex-wrap flex-xl-nowrap justify-content-center jutify-content-xl-start gap-1 p-0 g-0">
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc6} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Movie</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc7} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Movie</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc8} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Music</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc9} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Music</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc10} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Music</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <div className="d-md-inline-flex flex-wrap flex-xl-nowrap justify-content-center jutify-content-xl-start gap-1 p-0 g-0">
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc11} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Writing</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc12} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Writing</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc13} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Movie</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc14} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Movie</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc15} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Movie</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <div className="d-md-inline-flex flex-wrap flex-xl-nowrap justify-content-center jutify-content-xl-start gap-1 p-0 g-0">
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc16} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Music</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc17} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Music</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc18} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Writing</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc19} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Music</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc20} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Movie</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <div className="d-md-inline-flex flex-wrap flex-xl-nowrap justify-content-center jutify-content-xl-start gap-1 p-0 g-0">
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc21} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Music</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc22} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Writing</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc23} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Music</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc24} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Writing</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="carousel--content">
                                            <Card className="carousel--content--card">
                                                <Card.Img variant="top" src={imgSc25} />
                                                <Card.Body className="py-3 px-2">
                                                    <span className="px-3 py-1 text-small">Podcast</span>
                                                    <p className="text-small mt-3">
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                                                    </p>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="container-fluid p-0 g-0">
                                <Row className="p-0 g-0 bottom mt-4 mt-xl-0">
                                    <Col className="pt-5">
                                        <div class="carousel-indicators">
                                            <button type="button" data-bs-target="#carouselExampleControls" data-bs-slide-to="0" className="active rounded-circle" aria-current="true" aria-label="Slide 1"></button>
                                            <button type="button" data-bs-target="#carouselExampleControls" data-bs-slide-to="1" className="rounded-circle" aria-label="Slide 2"></button>
                                            <button type="button" data-bs-target="#carouselExampleControls" data-bs-slide-to="2" className="rounded-circle" aria-label="Slide 3"></button>
                                            <button type="button" data-bs-target="#carouselExampleControls" data-bs-slide-to="3" className="rounded-circle" aria-label="Slide 4"></button>
                                            <button type="button" data-bs-target="#carouselExampleControls" data-bs-slide-to="4" className="rounded-circle" aria-label="Slide 5"></button>
                                        </div>
                                    </Col>
                                    <Col className="col-auto ms-auto">
                                        <Stack direction="horizontal" gap={2} className="arrow">  
                                            <svg xmlns="http://www.w3.org/2000/svg" class="bi bi-arrow-left-circle" viewBox="0 0 16 16" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                                                <path fill-rule="evenodd" d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.5-.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"/>
                                            </svg>                          
                                            <svg xmlns="http://www.w3.org/2000/svg" class="bi bi-arrow-right-circle" viewBox="0 0 16 16" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                                                <path fill-rule="evenodd" d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM4.5 7.5a.5.5 0 0 0 0 1h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H4.5z"/>
                                            </svg>
                                        </Stack>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                    </div>
                </Row>
            </Container>
        </div>
    )
}

export default ShowCase;
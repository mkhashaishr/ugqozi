import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import { NavLink } from "react-router-dom";


const Footer = () => {
    return (
        <div className="footer-section">
            <Container>
                <Row className="px-0 g-0">
                    <Col md="6" xl="3" className="mb-5 mb-xl-0 footer-section--address">
                        <h4 className="text text-white fw-bold">Ugqozi.</h4>
                        <p className="text-small text-white">Proin id ligula dictum, convallis enim ut, facilisis massa. Mauris a nisi ut sapien blandit imperdiet sed id lacus. </p>
                    </Col>
                    <Col md="6" xl="3" className="mb-5 mb-xl-0 footer-section--link">
                        <h4 className="text text-white fw-bold">Explore</h4>
                        <ul className="text-small">
                            <li>
                                <NavLink to="/">
                                    Home
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/">
                                    Pricing
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/">
                                    Blog
                                </NavLink>
                            </li>
                        </ul>
                    </Col>
                    <Col md="6" xl="3" className="mb-5 mb-xl-0 footer-section--link">
                        <h4 className="text text-white fw-bold">Follow</h4>                        
                        <ul className="text-small">
                            <li>
                                <NavLink to="/">
                                    Home
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/">
                                    Pricing
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/">
                                    Blog
                                </NavLink>
                            </li>
                        </ul>
                    </Col>
                    <Col md="6" xl="3" className="mb-5 mb-xl-0 footer-section--contact">
                        <h4 className="text text-white fw-bold">Contact</h4>    
                        <ul className="text-small">
                            <li>2715 Ash Dr. San Jose, South Dakota 83475</li>
                            <li>(406) 555-0120</li>
                            <li>contact@ugqozi.com</li>
                        </ul>
                    </Col>
                </Row>
                <Row className="px-0 g-0 text-center footer-section--footer">
                    <span className="text-small text-white">Copyright © 2021 <b>Ugqozi</b>. All Rights Reserved</span>
                </Row>
            </Container>
        </div>
    )
}

export default Footer;
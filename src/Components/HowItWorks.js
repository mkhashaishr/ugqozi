import React from "react";
import { Container, Row, Stack } from "react-bootstrap";
import imgStep from "../Assets/Img/How it works/steps.svg";

const HowItWorks = () => {
    return (
        <div className="howItWorks-section">
            <Container>
                <Row className="px-0 g-0">
                    <h1 className="text-header mb-5 text-center">How it works</h1>
                    <Stack direction="horizontal" className="gap-1 gap-md-2 gap-xl-3 align-items-start">
                        <Stack className="align-items-end w-25 howItWorks-section--left">
                            <p className="text fw-bold howItWorks-section--left--step1">Step One</p>
                            <p className="text howItWorks-section--left--step2 lh-base">Phasellus tincidunt dignissim tellus, vel fermentum tellus semper vel. 
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam pretium 
                                at tortor et sodales. Cras posuere bibendum mi, ege
                            </p>
                            <p className="text fw-bold howItWorks-section--left--step3">Step Three</p>
                        </Stack>
                        <img src={imgStep} alt="Step" />
                        <Stack className="w-25 howItWorks-section--right">
                            <p className="text howItWorks-section--right--step1">
                                Vivamus hendrerit tincidunt urna sit amet placerat. Sed a venenatis arcu, at sollicitudin leo.
                                Morbi cursus feugiat sagittis. Mauris vehicula nunc vitae tristique porta. Duis blandit urna nonsea
                            </p>
                            <p className="text fw-bold howItWorks-section--right--step2">Step Two</p>
                            <p className="text howItWorks-section--right--step3">
                                Olutpat elit eros in metus. Duis mattis, lectus nec accumsan vulputate, ante leo condimentum arcu, non 
                                congue ante dui in tellus. Vivamus id ultricies lorem. Cras vitae lobortis ligula. Praesent
                            </p>
                        </Stack>
                    </Stack>
                </Row>
            </Container>
        </div>
    )
}

export default HowItWorks;
import React from "react";
import { Container, Row, Stack } from "react-bootstrap";

const Faq = () => {
    return (
        <div className="faq-section">
            <Container>
                <Row className="px-0 g-0">
                    <h1 className="text-header mb-5 text-center">FAQ</h1>
                </Row>
                <Row className="px-0 g-0">
                    <Stack gap={2}>
                        <div>
                            <p>
                                <a className="btn w-100 shadow-sm" style={{background: "white", borderRadius: "10px"}} data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    <div className="d-flex align-items-center" style={{height: "3.75rem"}}>
                                        <span className="text text-faq">Etiam faucibus elit a quam vestibulum lacinia. Phasellus at odio justo ?</span>
                                        <span className="ms-auto">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#3182CE" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                                                <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                            </svg>
                                        </span>
                                    </div>
                                </a>
                            </p>
                            <div className="collapse" id="collapseExample">
                                <div className="card card-body" style={{borderRadius: "10px"}}>
                                    <span className="text text-faq">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis minima vitae illum, deleniti facilis ipsam neque odit tenetur. Sit, voluptas. Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div>
                            <p>
                                <a className="btn w-100 shadow-sm" style={{background: "white", borderRadius: "10px"}} data-bs-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    <div className="d-flex align-items-center" style={{height: "3.75rem"}}>
                                        <span className="text text-faq">Nullam ultrices orci quis quam elementum suscipit vel et odio ?</span>
                                        <span className="ms-auto">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#3182CE" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                                                <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                            </svg>
                                        </span>
                                    </div>
                                </a>
                            </p>
                            <div className="collapse" id="collapseExample2">
                                <div className="card card-body" style={{borderRadius: "10px"}}>
                                    <span className="text text-faq">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis minima vitae illum, deleniti facilis ipsam neque odit tenetur. Sit, voluptas. Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div>
                            <p>
                                <a className="btn w-100 shadow-sm" style={{background: "white", borderRadius: "10px"}} data-bs-toggle="collapse" href="#collapseExample3" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    <div className="d-flex align-items-center" style={{height: "3.75rem"}}>
                                        <span className="text text-faq">Etiam faucibus elit a quam vestibulum lacinia. Phasellus at odio justo ?</span>
                                        <span className="ms-auto">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#3182CE" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                                                <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                            </svg>
                                        </span>
                                    </div>
                                </a>
                            </p>
                            <div className="collapse" id="collapseExample3">
                                <div className="card card-body" style={{borderRadius: "10px"}}>
                                    <span className="text text-faq">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis minima vitae illum, deleniti facilis ipsam neque odit tenetur. Sit, voluptas. Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div>
                            <p>
                                <a className="btn w-100 shadow-sm" style={{background: "white", borderRadius: "10px"}} data-bs-toggle="collapse" href="#collapseExample4" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    <div className="d-flex align-items-center" style={{height: "3.75rem"}}>
                                        <span className="text text-faq">Nullam ultrices orci quis quam elementum suscipit vel et odio ?</span>
                                        <span className="ms-auto">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#3182CE" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                                                <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                            </svg>
                                        </span>
                                    </div>
                                </a>
                            </p>
                            <div className="collapse" id="collapseExample4">
                                <div className="card card-body" style={{borderRadius: "10px"}}>
                                    <span className="text text-faq">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis minima vitae illum, deleniti facilis ipsam neque odit tenetur. Sit, voluptas. Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div>
                            <p>
                                <a className="btn w-100 shadow-sm" style={{background: "white", borderRadius: "10px"}} data-bs-toggle="collapse" href="#collapseExample5" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    <div className="d-flex align-items-center" style={{height: "3.75rem"}}>
                                        <span className="text text-faq">Etiam faucibus elit a quam vestibulum lacinia. Phasellus at odio justo ?</span>
                                        <span className="ms-auto">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#3182CE" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                                                <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                            </svg>
                                        </span>
                                    </div>
                                </a>
                            </p>
                            <div className="collapse" id="collapseExample5">
                                <div className="card card-body" style={{borderRadius: "10px"}}>
                                    <span className="text text-faq">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis minima vitae illum, deleniti facilis ipsam neque odit tenetur. Sit, voluptas. Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
                                    </span>
                                </div>
                            </div>
                        </div>
                    </Stack>
                </Row>
            </Container>
        </div>
    )
}

export default Faq;
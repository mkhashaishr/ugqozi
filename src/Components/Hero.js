import React from "react";
import { Container, Row, Col, Button, Stack } from "react-bootstrap";
import imgNet from "../Assets/Img/Hero/net.svg";
import imgHero from "../Assets/Img/Hero/hero.png";
import imgBgHero from "../Assets/Img/Hero/bg-hero.svg";
import imgGroup from "../Assets/Img/Hero/group.svg";

const Hero = () => {
    return (
        <>
            <div className="hero-section position-relative">
                <Container className="position-relative" style={{zIndex: "999"}}>
                    <Row className="px-0 g-0 align-items-center justify-content-between">
                        <Col md="6" xl="5" className="hero-section--text order-2 order-md-1 my-5 my-md-0">
                            <Stack>
                                <h1 className="text-header"><b>Ugqozi</b>, a place where stories bring talented people together .</h1>
                                <p className="text my-3">Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                                <div className="d-flex gap-2">
                                    <Button variant="primary" className="text text-white px-4 py-2">Write Ideas</Button>
                                    <Button className="text text-primary white border-0 px-4 py-2">Find Ideas</Button>
                                </div>
                            </Stack>
                        </Col>
                        <Col md="5" lg="4" xl="5" className="offset-xl-1 hero-section--img order-1 order-md-2">
                            <img src={imgHero} alt="Hero" className="img-fluid img-hero" />
                        </Col>
                    </Row>
                </Container>
                <img src={imgBgHero} alt="Net" className="img-fluid img-bg position-absolute start-0" />
                <img src={imgNet} alt="Net" className="img-fluid img-net position-absolute end-0" />
                <img src={imgGroup} alt="Group" className="img-group position-absolute" />
            </div>
        </>
    )
}

export default Hero;
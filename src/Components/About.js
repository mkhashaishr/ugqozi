import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import imgAbout from "../Assets/Img/About/about.png";
import imgBgAbout from "../Assets/Img/Hero/bg-hero.svg";

const About = () => {
    return (
        <>
            <div className="about-section position-relative">
                <Container className="position-relative" style={{zIndex: "999"}}>
                    <Row className="px-0 g-0 align-items-center justify-content-between">
                        <Col md="6" lg="5" className="order-2 order-md-1">
                            <h1 className="text-header mb-3">About Ugqozi</h1>
                            <p className="text">
                                Vestibulum sagittis venenatis nisi a varius. Aliquam gravida, magna at lacinia elementum, 
                                nisi lectus mattis metus, ac sodales turpis nisi nec orci. Nulla at erat iaculis, porttitor urna sed, aliquet mauris.
                                Ut aliquam libero at purus fringilla aliquet. Pellentesque semper odio eu suscipit dapibus. Interdum et malesuada fames 
                                ac ante ipsum primis in faucibus. Praesent sit ametas
                            </p>
                        </Col>
                        <Col md="5" lg="5" className="about-section--img order-1 order-md-2 mb-5 mb-md-0">
                            <img src={imgAbout} alt="About" className="img-fluid" />
                        </Col>
                    </Row>
                </Container>
                <img src={imgBgAbout} alt="Net" className="img-fluid img-bg position-absolute end-0" />
            </div>
        </>
    )
}

export default About;
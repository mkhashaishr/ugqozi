import React from "react";
import { NavLink } from "react-router-dom";
import { Button } from "react-bootstrap";
import imgLogo from "../Assets/Img/logo.png";

const Navbar = () => {
    return (
        <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-white shadow-sm p-md-3">
        <div className="container-fluid">
            <NavLink className="navbar-brand" to="/">
                <img src={imgLogo} alt="Web Logo" className="me-2"/>
                Ugqozi.
            </NavLink>
            <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
            >
            <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav link">
                <li className="nav-item">
                    <NavLink to="/" className="nav-link text-gray-900">Home</NavLink>
                </li>
                <li className="nav-item dropdown">
                    <NavLink className="nav-link dropdown-toggle text-gray-900" to="/" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Explore
                    </NavLink>
                    <ul className="dropdown-menu" aria-labelledby="navbarDarkDropdownMenuLink">
                        <li><NavLink className="dropdown-item" to="/#">Action</NavLink></li>
                        <li><NavLink className="dropdown-item" to="/">Another action</NavLink></li>
                        <li><NavLink className="dropdown-item" to="/">Something else here</NavLink></li>
                    </ul>
                </li>
                <li className="nav-item">
                    <NavLink to="/" className="nav-link text-gray-900">Pricing</NavLink>
                </li>
                <li className="nav-item dropdown">
                    <NavLink className="nav-link dropdown-toggle text-gray-900" to="/" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Resources
                    </NavLink>
                    <ul className="dropdown-menu" aria-labelledby="navbarDarkDropdownMenuLink">
                        <li><NavLink className="dropdown-item" to="/">Action</NavLink></li>
                        <li><NavLink className="dropdown-item" to="/">Another action</NavLink></li>
                        <li><NavLink className="dropdown-item" to="/">Something else here</NavLink></li>
                    </ul>
                </li>
            </ul>
            <div className="mx-auto"></div>
            <ul className="navbar-nav">
                <div className="nav-button d-flex gap-2">
                    <Button variant="primary" className="text white text-primary px-4">Login</Button>
                    <Button className="text text-primary text-white border-0 px-3">Register</Button>
                </div>
            </ul>
            </div>
        </div>
    </nav>  
    )
}

export default Navbar;
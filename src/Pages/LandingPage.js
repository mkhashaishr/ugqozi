import React, {useEffect} from "react";
import Navbar from "../Components/Navbar";
import Hero from "../Components/Hero";
import {Stack } from "react-bootstrap";
import About from "../Components/About";
import HowItWorks from "../Components/HowItWorks";
import ShowCase from "../Components/ShowCase";
import Footer from "../Components/Footer";
import Faq from "../Components/Faq";

const LandingPage = () => {

    useEffect (() => {
        document.title = `Ugqozi - a place where stories bring talented people together`
    })

    return (
        <div className="landing-page">
            <Stack>
                <Navbar />
                <Hero />
                <About />
                <HowItWorks />
                <ShowCase />
                <Faq />
                <Footer />
            </Stack>
        </div>
    )
}

export default LandingPage;